<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Annotation\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\Ticket;

class TicketController extends Controller
{
    /**
     * @Route("/ticket", name="ticket", methods={"GET"})
     */
    public function index()
    {
        return $this->render('ticket/index.html.twig');
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $this->getDoctrine()
            ->getRepository(Ticket::class)
            ->find($id);

        $entityManager->remove($product);
        $entityManager->flush();

        return $this->redirect('/');
    }

    /**
     * @Route("/ticket", name="create", methods={"POST"})
     */
    public function create(Request $request, ValidatorInterface $validator)
    {
        $title = $request->get('title');
        $content = $request->get('content');
        $author = $request->get('author');

        $entityManager = $this->getDoctrine()->getManager();

        $ticket = new Ticket();
        $ticket->setTitle($title);
        $ticket->setContent($content);
        $ticket->setAuthor($author);

        $errors = $validator->validate($ticket);

        if (count($errors) > 0) {
            /*
             * Uses a __toString method on the $errors variable which is a
             * ConstraintViolationList object. This gives us a nice string
             * for debugging.
             */
            $errorsString = (string) $errors;
    
            return new Response($errorsString);
        }

        $entityManager->persist($ticket);
        $entityManager->flush();

        return $this->redirect('/');
    }
}
