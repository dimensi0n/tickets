<?php
namespace App\DataFixtures;

use App\Entity\Ticket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // create 20 products! Bam!
        for ($i = 0; $i < 20; $i++) {
            $ticket = new Ticket();
            $ticket->setTitle("Ticket number" . $i);
            $ticket->setAuthor("root");
            $ticket->setContent($i);
            $manager->persist($ticket);
        }

        $manager->flush();
    }
}